//
//  TableViewCell.swift
//  todoApp
//
//  Created by Arnas Valiauga on 10/17/18.
//  Copyright © 2018 Arnas Valiauga. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var desc: UITextView!
    func setTask(task: Task) {
        title.text = task.title
        desc.text = task.description
    }
    
}
