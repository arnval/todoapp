//
//  Task.swift
//  todoApp
//
//  Created by Arnas Valiauga on 10/17/18.
//  Copyright © 2018 Arnas Valiauga. All rights reserved.
//

class Task {
    var title: String
    var description: String
    init(title: String, description: String) {
        self.title = title
        self.description = description
    }
}
