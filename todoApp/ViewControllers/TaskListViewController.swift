//
//  ViewController.swift
//  todoApp
//
//  Created by Arnas Valiauga on 10/17/18.
//  Copyright © 2018 Arnas Valiauga. All rights reserved.
//

import UIKit

class TaskListViewController: UITableViewController, TaskHandler {

    var taskList: [Task] = [Task(title: "First Task",description: "Do something")]
    
    @IBOutlet var taskTableView: UITableView!
    func addTask(title: String, description: String) {
        taskList.append(Task(title: title, description: description))
        taskTableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return taskList.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TaskCell", for: indexPath) as! TableViewCell
        cell.setTask(task: taskList[indexPath.row])
        return cell
    }


}

