//
//  AddTaskViewController.swift
//  todoApp
//
//  Created by Arnas Valiauga on 10/17/18.
//  Copyright © 2018 Arnas Valiauga. All rights reserved.
//

import UIKit

class AddTaskViewController: UIViewController {

    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var descriptionTextView: UITextView!
    @IBOutlet weak var doneButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func done(_ sender: UIButton) {
       let controller = self.navigationController?.viewControllers[0] as! TaskListViewController
        controller.addTask(title: titleTextView.text!, description: descriptionTextView.text!)
        self.navigationController?.popViewController(animated: true)
    }

}
