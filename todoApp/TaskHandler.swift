//
//  TaskHandler.swift
//  todoApp
//
//  Created by Arnas Valiauga on 10/17/18.
//  Copyright © 2018 Arnas Valiauga. All rights reserved.
//

protocol TaskHandler {
    func addTask(title: String, description: String)
}
